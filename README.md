# Battery Plug Notifier

A simple notifier to hopefully extend battery life.

## Why?

A lot laptop batteries use [Lithium-ion
batteries](https://en.wikipedia.org/wiki/Lithium-ion_battery).

There is a simple technique to help extend battery life, by keeping the charge
not so high, and not so low.

Some laptops already include a feature to help with this, namely thinkpads with
[tp_smapi](https://www.thinkwiki.org/wiki/Tp_smapi), but most do not have that
feature.

## How?

A work-around not having kernel level access to start/stop charging is to do it
manually, but as humans it is hard to remember to monitor the percentage.

This notifier helps with the task by sending notifications at those moments.

## Install

The script is intended to be run in the background, started from the desktop
environment.

The program relies on the following dependencies: acpi, notify-send, and
logger.
On a Debian system, it is about installing related packages, e.g. `apt install
acpi libnotify-bin bsdutils`.

### Manually

* Copy the `battery-plug-notifier.sh` to one directory in your `$PATH`.
* Make the program start after the desktop starts.
  E.g. If you use GNOME, and have `gnome-tweak-tool` installed just add the
  script to your 'Startup applications'.
